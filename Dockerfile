FROM node:12-alpine as build-frontend
WORKDIR /app/frontend
# add `/app/node_modules/.bin` to $PATH
ENV PATH /app/frontend/node_modules/.bin:$PATH
# copy project files and folders to the current working directory (i.e. 'app' folder)
COPY ./frontend /app/frontend
RUN npm install \
    && npm install @vue/cli@3.7.0 -g \
    && npm audit fix

RUN npm run build

FROM centos/python-38-centos7
# Ensure that Python outputs everything that's printed inside
# the application rather than buffering it.
ENV PYTHONUNBUFFERED 1

USER root
WORKDIR /app

# By copying over requirements first, we make sure that Docker will cache
# our installed requirements rather than reinstall them on every build
COPY /nota/requirements.txt /app/requirements.txt

RUN rpm -Uvh https://download.postgresql.org/pub/repos/yum/reporpms/EL-7-x86_64/pgdg-redhat-repo-latest.noarch.rpm
RUN yum install -y postgresql10-devel && yum clean all
RUN pip3 install -r requirements.txt

# Now copy in our code
COPY . /app
COPY --from=build-frontend /app/frontend/dist /app/frontend/dist
VOLUME /app/static

RUN chgrp -R 0 /app && \
    chmod -R g=u /app

WORKDIR /app/nota
RUN chmod +x /app/nota/docker-entrypoint.sh
ENTRYPOINT ["/app/nota/docker-entrypoint.sh"]
CMD ["gunicorn"]
