import axios from "axios";
import router from "./router/index";
import store from "./store/index"
import { Message } from "element-ui";

const instance = axios.create({
  baseURL: process.env.VUE_APP_DOMAIN,
});

instance.interceptors.response.use(
  (response) => {
    return response;
  },
  (error) => {
    if (error.response && error.response.status >= 400 && error.response.status < 500) {
        store.commit("resetData");  // Reset user info
        router.push('/login') // Reset token in localstorage (browser cookies)
        Message.error({message: error.response.data.detail, showClose: true})
    }
    if (error.response && error.response.status >= 500) {
        store.commit("resetData");  // Reset user info
        router.push('/login') // Reset token in localstorage (browser cookies)
        Message.error({message: `Error with status ${error.response.status} has occurred`, showClose: true})
    }
    if (error.status !== 200 || error.response == null) {
      console.error(error);
    }
    return Promise.reject(error);
  }
);

export default instance;
