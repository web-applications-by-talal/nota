import Vue from "vue";
import VueDraggableResizable from 'vue-draggable-resizable'
import 'vue-draggable-resizable/dist/VueDraggableResizable.css' // optionally import default styles
import App from "./App.vue";
import Element from "element-ui";
import locale from "element-ui/lib/locale/lang/en";
import "element-ui/lib/theme-chalk/index.css";
import router from './router/index';
import store from './store';
import Grid from 'vue-js-grid'

locale.el.pagination.pagesize = "";

Vue.use(Element, { locale });
Vue.use(Grid)
Vue.component('vue-draggable-resizable', VueDraggableResizable)

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount("#app");
