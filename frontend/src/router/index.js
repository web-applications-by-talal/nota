import Vue from 'vue'
import Router from 'vue-router'
import Home from '../components/home/Home.vue'

Vue.use(Router);

const router = new Router({
  mode: 'hash',
  routes: [
    {
      path: '/',
      component: Home,
      name: '/',
      props: true,
      children: [
        {
          path: 'upload',
          component: () => import(/* webpackChunkName: "upload" */ '../components/home/tabs/Upload.vue'),
          name: 'upload',
          props: true
        }
      ]
    },
    {
      path: '/notes/:id',
      name: 'notes',
      props: true,
      component: () => import(/* webpackChunkName: "noteslist" */ '../components/NotesList.vue'),
      meta: {
        header: 1,
        name: "Notes"
      }
    },
    {
      path: '/login',
      name: 'login',
      component: () => import(/* webpackChunkName: "login" */ '../components/Login.vue'),
      meta: {
        header: 1,
        name: "Login"
      }
    },
    {
      path: '/404',
      alias: '*',
      name: "notFound",
      component: () => import(/* webpackChunkName: "notfound" */ '../components/TheNotFound.vue')
    }
  ]
});

router.beforeEach((to, from, next) => {
  if (to.name === "upload") {
    if (!localStorage.getItem("token")){
      router.push({ 
        name: 'login',
        params: {
          returnTo: to.path,
          query: to.query,
        },
      });
    }
  }

  next()
})


export default router;