import Vue from 'vue'
import Vuex from 'vuex'
import {getNoteBooksURL, createNoteBooksURL} from '../utils/urls.js'
import notesapi from "../notesapi";

Vue.use(Vuex)

const store = new Vuex.Store({
    state: {
        token: localStorage.getItem('token') || null,
        username: localStorage.getItem('username') || null,
        isLoggedIn: localStorage.getItem('isLoggedIn') || false,
        isLoadingNotebooks: false,
        notebooks: []
    },
    mutations: {
        updateData(state, {token, username}){
            localStorage.setItem("token", token)
            localStorage.setItem("isLoggedIn", true)
            localStorage.setItem("username", username)
            state.token = localStorage.getItem("token")
            state.username = localStorage.getItem("username")
            state.isLoggedIn = localStorage.getItem("isLoggedIn")
        },
        resetData(state){
            localStorage.removeItem("token")
            localStorage.removeItem("isLoggedIn")
            localStorage.removeItem("username")
            state.token = localStorage.getItem("token")
            state.username = localStorage.getItem("username")
            state.isLoggedIn = false
        },
        updateLoading(state, {isLoading}){
            state.isLoadingNotebooks = isLoading
        },
        updateNotebooks(state, data){
            state.notebooks = data
        },
        setNotebookGridPositions(state, notebooks){
            state.notebooks = notebooks
        }
    },
    actions: {
        async fetchNotebooks({commit}){
            commit('updateLoading', {"isLoading": true})

            const response = await notesapi.get(getNoteBooksURL);
            commit('updateNotebooks', response.data)

            commit('updateLoading', {"isLoading": false})
        },

        async bulkCreateNotebooks({state}, notebooks){
            const headers = { "Content-Type": "application/json", "Authorization": `Token ${state.token}`}
            await notesapi.post(createNoteBooksURL, notebooks,
                {headers: headers});
        }
    }
})

export default store;