# Nota

Designed as a solution to view and explore notes exported from [OmniOutliner](https://www.omnigroup.com/omnioutliner/) as Dynamic HTML folders.
An admin user can import these notes into Nota, making them accessible to the public.

The web app was requested by my brother since he was looking for an **interactive and highly efficient** way to access his +20,000 lines of notes from the internet and share them with his colleagues in medicine.

# Setting Up the Dev environment:

### Run Django Server locally (without Docker):

1. cd nota
2. Run `pip3 install virtualenv`
3. Run `virtualenv venv`
4. Run `source venv/bin/activate`
5. Run `pip3 install -r requirements.txt`
6. Set env variable `export DJANGO_SETTINGS_MODULE=nota.settings.development`
7. Run `python3 manage.py migrate`
8. Run `python3 manage.py runserver`

### Run Vue serve:

1. cd frontend
2. Run `npm i`
2. Run `npm run serve`

---
The website is available at http://notaapp.site/

