#!/bin/sh
# Source: https://github.com/lukin0110/docker-django-boilerplate/blob/master/Dockerfile
# Exit immediately if a command exits with a non-zero status.
# http://stackoverflow.com/questions/19622198/what-does-set-e-mean-in-a-bash-script
set -e

# Check if the required PostgreSQL environment  variables are set

# Used by docker-entrypoint.sh to start the dev server
# If not configured you'll receive this: CommandError: "0.0.0.0:" is not a valid port number or address:port pair.
# [ -z "$PORT" ] && echo "ERROR: Need to set PORT. E.g.: 8000" && exit 1;

# [ -z "$POSTGRES_DB_NAME" ] && echo "ERROR: Need to set POSTGRES_DB_NAME" && exit 1;
# [ -z "$POSTGRES_USER" ] && echo "ERROR: Need to set POSTGRES_USER" && exit 1;
# [ -z "$POSTGRES_PASSWORD" ] && echo "ERROR: Need to set POSTGRES_PASSWORD" && exit 1;

# # Used by uwsgi.ini file to start the wsgi Django application
# [ -z "$WSGI_MODULE" ] && echo "ERROR: Need to set WSGI_MODULE. E.g.: hello.wsgi:application" && exit 1;


# Define help message
show_help() {
    echo """
Usage: docker run <imagename> COMMAND
Commands
dev      : Start a normal Django development server
bash     : Start a bash shell
manage   : Start manage.py
setup    : Setup the initial database. Configure \$POSTGRES_DB_NAME in docker-compose.yml
lint     : Run pylint
test     : Run test
python   : Run a python command
shell    : Start a Django Python shell
gunicorn : Run gunicorn server
celery   : Run celery worker
help     : Show this message
"""
}


# Run
case "$1" in
    dev)
        echo "Running Development Server on 0.0.0.0:${WEB_PORT}"
        python3 manage.py runserver 0.0.0.0:${WEB_PORT}
    ;;
    bash)
        /bin/sh
    ;;
    manage)
        python3 manage.py "${@:2}"
    ;;
    setup)
        python3 manage.py migrate --noinput
        python3 manage.py collectstatic --noinput
    ;;
    lint)
        pylint "${@:2}"
    ;;
    test)
        python3 manage.py test
    ;;
    python)
        python3 "${@:2}"
    ;;
    shell)
        python3 manage.py shell
    ;;
    gunicorn)
        echo "Running App (Gunicorn)..."
        echo ls -la
        ./docker-entrypoint.sh setup
        gunicorn --timeout 120 --env DJANGO_SETTINGS_MODULE=${DJANGO_SETTINGS_MODULE} --access-logfile - --workers 4 --bind=0.0.0.0:${WEB_PORT}  nota.wsgi:application
    ;;
    celery)
        echo "Running App (Celery)..."
        celery -A nota worker --concurrency=5 --loglevel=info
    ;;
    *)
        show_help
    ;;
esac