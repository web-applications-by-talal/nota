from os import remove
from celery import shared_task
from lxml import etree

from django_drf_filepond.models import TemporaryUpload
from django.contrib.auth.models import User

from nota.settings.base import DJANGO_DRF_FILEPOND_FILE_STORE_PATH
from .utils import previous_and_next, unzip_dynamic_html_file, get_html_text, extract_note_info
from .models import Notes, Attachment, NoteBook


@shared_task(bind=True)
def import_notes_task(self, temp_file_id, user_id, temp_file_name, temp_file_path):

    # Accepts a zip folder containing: index.html and images
    data, errors = import_dynamic_html(temp_file_path, temp_file_name)

    display_name = temp_file_name.split('.')[0]
    notebook = NoteBook.objects.create(name=display_name, user=User.objects.get(id=user_id))

    create_notes_tree(data, notebook)

    # Delete Temporary folder
    TemporaryUpload.objects.get(pk=temp_file_id).delete()


def import_dynamic_html(temp_file_path, file_name):
    """Imports the nodes from the HTML tree. Following cases include:
    1- Foot-note text and Image
    2- Image only
    3- Foot-note Text only
    4- Main text only"""
    archive, index_html, file_path = unzip_dynamic_html_file(temp_file_path, file_name)

    data = []
    errors = []
    parser = etree.HTMLParser(recover=False)
    tree = etree.parse(index_html, parser)
    content = tree.xpath('//table[@data-level]')  # Get table hierarchy

    # Iterate each node in tree
    for item in content:
        # Get node depth
        key = int(item.attrib['data-level'])

        images, errors, is_footnote, is_img = extract_note_info(item, archive, file_path)

        # Get node's text elements and concatenate them
        row_text = get_html_text(item, is_footnote)

        # Append node information to 'data' list
        # Foot notes are pushed one level deeper
        if is_footnote and is_img:
            data.append((key + 1, 'foot-note', 'image', row_text, images))
        elif is_footnote and row_text and row_text is not None and row_text.strip() != '':
            data.append((key + 1, 'foot-note', 'not-image', row_text))

        elif is_img:
            data.append((key, 'not-foot-note', 'image', row_text, images))
        elif row_text and row_text is not None and row_text.strip() != '':
            data.append((key, 'not-foot-note', 'not-image', row_text))

    archive.close()
    remove(DJANGO_DRF_FILEPOND_FILE_STORE_PATH + '/' + file_path + '/index.html')
    return data, errors


def create_notes_tree(data, notebook):
    """Creates notes in tree structure"""
    parents_dict = {}
    for _, item, nxt in previous_and_next(data):
        # Create last object
        if not nxt:
            create_note(item, notebook, parents_dict)
            continue

        # Create parent nodes
        if nxt[0] > item[0]:
            parent = create_note(item, notebook, parents_dict)
            parents_dict[item[0]] = parent
        # Create child nodes
        else:
            create_note(item, notebook, parents_dict)


def create_note(item, notebook, parents_dict):
    """Create note based on condition"""
    # Just Text
    if item[1] == 'not-foot-note' and item[2] == 'not-image':
        note = Notes.objects.create(notebook=notebook, body=item[3], parent=parents_dict.get(item[0] - 1))
    # Just Image
    elif item[1] == 'not-foot-note' and item[2] == 'image':
        note = Notes.objects.create(notebook=notebook, parent=parents_dict.get(item[0] - 1),
                                    body=item[3])
        for name, src in item[4]:
            Attachment.objects.create(name=name, file=src, note=note)

    # Just foot note
    elif item[1] == 'foot-note' and item[2] == 'not-image':
        note = Notes.objects.create(notebook=notebook, body=item[3], parent=parents_dict.get(item[0] - 1),
                                    is_footnote=True)
    # Both foot note and image
    elif item[1] == 'foot-note' and item[2] == 'image':
        note = Notes.objects.create(notebook=notebook, parent=parents_dict.get(item[0] - 1),
                                    is_footnote=True, body=item[3])
        for name, src in item[4]:
            Attachment.objects.create(name=name, file=src, note=note)
    return note
