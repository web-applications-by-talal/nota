import magic
from django.conf import settings
from django.core.exceptions import ValidationError
from django.utils.deconstruct import deconstructible
from django.template.defaultfilters import filesizeformat
from django.db import models
from mptt.models import MPTTModel, TreeForeignKey
from api.storage import VariableFileSystemStorage


class Base(models.Model):
    """A base class with essential properties to be reused"""
    name = models.CharField(max_length=254, null=True, blank=True)
    tag = models.CharField(max_length=254, null=True, blank=True, unique=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        """Meta class for Base model"""
        abstract = True


@deconstructible
class FileValidator:
    """Validates file size and type"""
    error_messages = {
     'max_size': ("Ensure this file size is not greater than %(max_size)s."
                  " Your file size is %(size)s."),
     'min_size': ("Ensure this file size is not less than %(min_size)s. "
                  "Your file size is %(size)s."),
     'content_type': "Files of type %(content_type)s are not supported.",
    }

    def __init__(self, max_size=None, min_size=None, content_types=()):
        self.max_size = max_size
        self.min_size = min_size
        self.content_types = content_types

    def __call__(self, data):
        if self.max_size is not None and data.size > self.max_size:
            params = {
                'max_size': filesizeformat(self.max_size),
                'size': filesizeformat(data.size),
            }
            raise ValidationError(self.error_messages['max_size'],
                                  'max_size', params)

        if self.min_size is not None and data.size < self.min_size:
            params = {
                'min_size': filesizeformat(self.min_size),
                'size': filesizeformat(data.size)
            }
            raise ValidationError(self.error_messages['min_size'],
                                  'min_size', params)

        if self.content_types:
            content_type = magic.from_buffer(data.read(), mime=True)
            data.seek(0)

            if content_type not in self.content_types:
                params = { 'content_type': content_type }
                raise ValidationError(self.error_messages['content_type'],
                                      'content_type', params)

    def __eq__(self, other):
        return (
            isinstance(other, FileValidator) and
            self.max_size == other.max_size and
            self.min_size == other.min_size and
            self.content_types == other.content_types
        )


class NoteBook(Base):
    """A NoteBook class to hold Notes objects"""
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    is_private = models.BooleanField(default=False)
    grid_position = models.IntegerField(default=0)

    def __str__(self):
        return str(self.name)


class Notes(MPTTModel, Base):
    """A Notes class to hold bodies of text"""
    notebook = models.ForeignKey(NoteBook, on_delete=models.CASCADE, related_name='notes', null=True, blank=True)
    parent = TreeForeignKey('self', on_delete=models.CASCADE, null=True, blank=True)
    body = models.TextField(null=True, blank=True)
    is_footnote = models.BooleanField(default=False, null=True, blank=True)

    class Meta:
        """Meta class for Notes model"""
        verbose_name = 'Notes'
        verbose_name_plural = 'Notes'

    def __str__(self):
        return str(self.body)


class Attachment(Base):
    """A Attachment class to hold file uploads"""
    # Attachment max_size is 10megabytes
    validate_file = FileValidator(max_size=10000000,
                                  content_types=('application/pdf', 'image/jpeg', 'image/tiff', 'image/jpeg',))
    file_storage = VariableFileSystemStorage()
    file = models.FileField(storage=file_storage, validators=[validate_file], max_length=255)
    note = models.ForeignKey(Notes, on_delete=models.CASCADE, null=True, blank=True, related_name="attachments")

    def __str__(self):
        return str(self.name)
