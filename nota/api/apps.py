from django.apps import AppConfig


class ApiConfig(AppConfig):
    """Declares the Django App name"""
    name = 'api'
