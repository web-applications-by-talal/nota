from django.contrib import admin
from mptt.admin import DraggableMPTTAdmin
from api.models import Notes, Attachment, NoteBook


# Register your models here.


@admin.register(Notes)
class NotesAdmin(DraggableMPTTAdmin, admin.ModelAdmin):
    """Notes Admin page representation"""
    list_display = ("tree_actions", "indented_title", "is_footnote", "has_image", "id", "notebook")
    search_fields = ("id", "body")

    def has_image(self, obj):
        """returns true if object has image"""
        attachment = Attachment.objects.filter(note=obj)
        if attachment:
            return True
        return False

    def formfield_for_foreignkey(self, db_field, request=None, **kwargs):
        field = super(NotesAdmin, self).formfield_for_foreignkey(db_field, request, **kwargs)

        if db_field.name == 'parent':
            parent_ids = Notes.objects.filter(parent_id__isnull=False)
            field.queryset = Notes.objects.filter(notes__in=parent_ids).distinct()

        return field


@admin.register(Attachment)
class AttachmentAdmin(admin.ModelAdmin):
    """Notes Admin page representation"""
    list_display = ("id", "name", "file")
    search_fields = ("id", "name")


@admin.register(NoteBook)
class NoteBookAdmin(admin.ModelAdmin):
    """NoteBook Admin page representation"""
    list_display = ("id", "name")
    search_fields = ("id", "name")
