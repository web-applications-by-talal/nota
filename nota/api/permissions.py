from django.contrib.auth.models import Permission
from rest_framework.permissions import BasePermission, SAFE_METHODS


class NoteBookCreatePermission(BasePermission):
    """Only allow admins to create and update"""
    def has_permission(self, request, view):
        if request.method in SAFE_METHODS:
            return True
        else:
            permission = Permission.objects.get(codename='add_notebook')
            return request.user.has_perm('api.{codename}'.format(codename=permission.codename))


class NoteBookDeletePermission(BasePermission):
    """Only allow admins to delete"""
    def has_permission(self, request, view):
        if request.method in SAFE_METHODS:
            return True
        else:
            permission = Permission.objects.get(codename='delete_notebook')
            return request.user.has_perm('api.{codename}'.format(codename=permission.codename))


class ReadOnly(BasePermission):
    """Returns True if request is in SAFE methods"""
    def has_permission(self, request, view):
        return request.method in SAFE_METHODS
