import pytz
from django.utils.timezone import localtime

from rest_framework import serializers
from .models import Notes, Attachment, NoteBook


class AttachmentSerializer(serializers.ModelSerializer):
    """Serializer for the attachment model"""
    id = serializers.IntegerField(read_only=True)

    class Meta:
        """Meta class for Attachment serializer"""
        model = Attachment
        fields = ['id']


class NotesSerializer(serializers.ModelSerializer):
    """Serializer for the Notes Model"""
    id = serializers.IntegerField(read_only=True)
    is_leaf = serializers.SerializerMethodField(read_only=True)
    has_image = serializers.SerializerMethodField(read_only=True)
    attachments = AttachmentSerializer(read_only=True, many=True)  # Related name field
    created_at = serializers.SerializerMethodField(read_only=True)

    class Meta:
        """Meta class for Notes serializer"""
        model = Notes
        fields = ['id', 'body', 'is_footnote', 'is_leaf', 'has_image', 'attachments', 'created_at']

    def get_is_leaf(self, obj):
        """Returns true if node is leaf"""
        return obj.is_leaf_node()

    def get_has_image(self, obj):
        """returns true if object has image"""
        attachment = Attachment.objects.filter(note=obj)
        if attachment:
            return True
        return False

    def get_created_at(self, obj):
        """Localize date to NY time"""
        tz = pytz.timezone('America/New_York')
        return localtime(obj.created_at, timezone=tz).strftime(format='%Y-%m-%d %H:%M:%S')


class NoteBookSerrializer(serializers.ModelSerializer):
    """Serializer for the Notebooks Model"""
    notes = serializers.SerializerMethodField(read_only=True)
    id = serializers.IntegerField()
    created_at = serializers.SerializerMethodField(read_only=True)

    class Meta:
        """Meta class for NoteBook serializer"""
        model = NoteBook
        fields = ['id', 'name', 'grid_position', 'is_private', 'user', 'created_at', 'notes']

    def get_notes(self, obj):
        """Limits response to 3 objects"""
        return NotesSerializer(obj.notes.all()[:3], many=True).data

    def get_created_at(self, obj):
        """Localize date to NY time"""
        tz = pytz.timezone('America/New_York')
        return localtime(obj.created_at, timezone=tz).strftime(format='%Y-%m-%d %H:%M:%S')
