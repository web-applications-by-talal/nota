"""
A place to declare all your application url patterns, to be imported in the global urls.py
"""
from django.urls import path
from rest_framework import routers
from api.filepond.views import CustomProcessView, CustomRevertView
from api.views import NotesCreateView, NotesListView, NotesRetrieveView, AttachmentRetrieveView, NoteBookListView,\
    NotesBookDestroyView, NoteBookCreateView

# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.

router = routers.DefaultRouter()

urlpatterns = [
    path('notebooks/', NoteBookListView.as_view(), name='notebooks-list'),
    path('notebooks/new/', NoteBookCreateView.as_view(), name='notebooks-create'),
    path('notebooks/<int:pk>/', NotesBookDestroyView.as_view(), name='notebooks-destroy'),
    path('notes/', NotesListView.as_view(), name='notes-list'),
    path('notes/new/', NotesCreateView.as_view(), name='notes-create'),
    path('notes/<int:pk>/', NotesRetrieveView.as_view(), name='notes-retrieve'),
    path('attachments/<int:pk>/', AttachmentRetrieveView.as_view(), name='attachments-retrieve'),
    path('fp/process/', CustomProcessView.as_view(), name='custom-fp-process'),
    path('fp/revert/', CustomRevertView.as_view(), name='fp-revert')
]

urlpatterns += router.urls
