"""utils methods"""
from itertools import tee, islice, chain
from zipfile import ZipFile
import magic
from rest_framework.exceptions import ParseError, UnsupportedMediaType
from django_drf_filepond.models import TemporaryUpload
from nota.settings.base import DJANGO_DRF_FILEPOND_FILE_STORE_PATH


def previous_and_next(iterable):
    """Gets previous and next item from iterable"""
    prevs, items, nexts = tee(iterable, 3)
    prevs = chain([None], prevs)
    nexts = chain(islice(nexts, 1, None), [None])
    return zip(prevs, items, nexts)


def unzip_dynamic_html_file(file_path, file_name):
    """Unzips a file and returns it and the root html"""
    archive = ZipFile(file_path, 'r')
    file_path = file_name.split('.')[0] + '.html'
    try:
        index_html = archive.extract(member=file_path + '/index.html', path=DJANGO_DRF_FILEPOND_FILE_STORE_PATH)
    except KeyError as err:
        raise ParseError(err)

    return archive, index_html, file_path


def extract_file(data):
    """Extract file from file pond temp"""
    if 'file' not in data:
        raise ParseError("Missing 'file' field.")
    if 'name' not in data:
        raise ParseError("Missing 'name' field.")

    file_id = data['file']
    try:
        temp_file = TemporaryUpload.objects.get(upload_id=file_id)
    except TemporaryUpload.DoesNotExist as err:
        raise ParseError(err)

    return temp_file


def validate_file_type(temp_file):
    """Validates file mime type and returns it"""
    temp_file_path = temp_file.get_file_path()
    file_type = magic.from_buffer(open(temp_file_path, "rb").read(2048))

    if "zip" not in file_type.lower():
        raise UnsupportedMediaType(media_type=file_type)

    return temp_file_path


def has_decorated_text(el):
    """Returns true if element has text-decoration inside style element"""
    has_decoration = False
    for child in el.getchildren():
        if 'text-decoration' in child.attrib.get('style', '') or 'font' in child.attrib.get('style', ''):
            has_decoration = True
    return has_decoration


def get_html_text(item, is_footnote):
    """Returns formatted text from dynamic html"""
    row_text = ""
    for el in item.iter():
        if el.text:
            has_decoration = has_decorated_text(el)

            # Append decorated text without new-line
            if has_decoration:
                row_text += str(el.text)

            # Add new line to foot-notes
            elif is_footnote:
                if el.tail:
                    row_text += str(el.text) + str(el.tail) + "\n"
                else:
                    row_text += str(el.text) + "\n"
            else:
                if el.tail:
                    row_text += str(el.text) + str(el.tail)
                else:
                    row_text += str(el.text)

    # Strip text after concatenation
    row_text = row_text.strip()
    return row_text


def extract_note_info(item, archive, file_path):
    """Extracts note text and images"""
    # Initialize Variables
    is_footnote = False
    is_img = False
    images = []
    errors = []

    # Check if texts are foot-notes or contain images
    for desc in item.iter():
        # check is foot-note
        if desc.attrib.get('class', '') == 'note':
            is_footnote = True

        # Then check text is not empty
        if not (desc.text and desc.text is not None and desc.text.strip() != '') and not desc.tag == "img":
            continue

        # check is image
        if desc.tag == "img":
            src = desc.attrib.get('src')

            # Exclude use-less images
            if any(ext in src
                   for ext in ['OONote', 'Mixed', 'LeafRowHandle', 'Expanded', 'Collapsed', 'Checked']):
                continue

            is_img = True
            try:
                src_name = src
                src = archive.extract(member=file_path + '/' + src,
                                      path=DJANGO_DRF_FILEPOND_FILE_STORE_PATH)
                images.append((src_name, src))
            except KeyError as err:
                errors.append(str(err))
                continue

    return images, errors, is_footnote, is_img
