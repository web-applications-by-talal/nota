from django_drf_filepond.views import ProcessView, RevertView
from rest_framework.exceptions import ParseError
from ..permissions import NoteBookCreatePermission, NoteBookDeletePermission


class CustomProcessView(ProcessView):
    """Overwrites ProcessView to limit upload size and add Permission Class"""
    permission_classes = [NoteBookCreatePermission]

    def post(self, request):
        """Limit file upload to 50 MB"""
        if request.data['filepond'].size > 50000000:
            raise ParseError("{file} too large. File must be smaller than 50MB".format(file=request.data['filepond']))

        return super(CustomProcessView, self).post(request)


class CustomRevertView(RevertView):
    """Overwrites RevertView to add Permission Class"""
    permission_classes = [NoteBookDeletePermission]
