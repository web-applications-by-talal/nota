import logging

import magic
from django.http import HttpResponse
from rest_framework import generics
from rest_framework.exceptions import ParseError
from rest_framework.response import Response

from .models import Notes, Attachment, NoteBook
from .permissions import NoteBookCreatePermission, NoteBookDeletePermission, ReadOnly
from .serializers import NotesSerializer, AttachmentSerializer, NoteBookSerrializer
from .tasks import import_notes_task
from .utils import extract_file, validate_file_type

# Logger Setup
logging.basicConfig(format='%(asctime)s.%(msecs)03d %(levelname)s {%(module)s} [%(funcName)s] %(message)s',
                    datefmt='%Y-%m-%d,%H:%M:%S', level=logging.INFO)
logger = logging.getLogger(__name__)


class NoteBookListView(generics.ListAPIView):
    """List NoteBook View"""
    queryset = NoteBook.objects.all().order_by('grid_position')
    serializer_class = NoteBookSerrializer
    permission_classes = [ReadOnly]


class NoteBookCreateView(generics.CreateAPIView):
    """List NoteBook View"""
    queryset = NoteBook.objects.all().order_by('grid_position')
    serializer_class = NoteBookSerrializer
    permission_classes = [NoteBookCreatePermission]

    def create(self, request, *args, **kwargs):
        is_many = isinstance(request.data, list)

        if is_many:
            serializer = NoteBookSerrializer(data=request.data, many=True)
            serializer.is_valid(raise_exception=True)

            for data in serializer.validated_data:
                NoteBook.objects.filter(id=data.pop('id')).update(**data)

            return Response(NoteBookSerrializer(self.get_queryset(), many=True).data)
        else:
            return super(NoteBookCreateView, self).create(request, args, kwargs)


class NotesBookDestroyView(generics.DestroyAPIView):
    """Destroy Notebook along with all the Notes linked to it"""
    queryset = Notes.objects.all()
    serializer_class = NoteBookSerrializer
    permission_classes = [NoteBookDeletePermission]

    def destroy(self, request, *args, **kwargs):
        notebook_id = kwargs.get('pk', None)

        try:
            notebook = NoteBook.objects.get(id=notebook_id)
        except (NoteBook.DoesNotExist, NoteBook.MultipleObjectsReturned) as err:
            raise ParseError(err)

        notes = Notes.objects.filter(notebook=notebook)
        if notes:
            notes.delete()

        notebook.delete()
        return Response(status=204)


class NotesListView(generics.ListAPIView):
    """List Notes View"""
    queryset = Notes.objects.filter(level=0)
    serializer_class = NotesSerializer
    permission_classes = [ReadOnly]

    def list(self, request, *args, **kwargs):
        filter_keyword = request.GET.get("filter_keyword", None)
        notebook_id = request.GET.get('notebook_id', None)
        exact_filter = request.GET.get('exact_filter', None)

        try:
            int(notebook_id)
        except (ValueError, TypeError):
            raise ParseError("{0} is not a valid ID".format(notebook_id))

        if not filter_keyword:
            queryset = Notes.objects.filter(notebook_id=notebook_id, level=0)
            return Response(NotesSerializer(queryset, many=True).data)

        if filter_keyword:
            if exact_filter:
                filtered_notes = Notes.objects.filter(body__contains=filter_keyword, notebook_id=notebook_id) \
                    .get_ancestors(include_self=True).values('body', 'id', 'level')
            else:
                filtered_notes = Notes.objects.filter(body__icontains=filter_keyword, notebook_id=notebook_id)\
                    .get_ancestors(include_self=True).values('body', 'id', 'level')

            return Response(filtered_notes)

        return super(NotesListView, self).list(request, args, kwargs)


class NotesCreateView(generics.CreateAPIView):
    """Import/Export Notes View"""
    queryset = Notes.objects.all()
    serializer_class = NotesSerializer
    permission_classes = [NoteBookCreatePermission]

    def create(self, request, *args, **kwargs):
        temp_file = extract_file(request.data)
        temp_file_name = request.data['name']
        temp_file_path = validate_file_type(temp_file)

        try:
            # Invoke notes import celery task
            import_notes_task.delay(temp_file.pk, request.user.id, temp_file_name, temp_file_path)
        except import_notes_task.OperationalError as exc:
            logger.exception('Celery task raised: %r', exc)

        return Response({
            "message": "{file_name} is being imported in the background. "
                       "Please allow some time for import to complete.".format(file_name=temp_file_name)
        })


class NotesRetrieveView(generics.RetrieveAPIView):
    """Retrieve Notes View"""
    queryset = Notes.objects.filter(level=0)
    serializer_class = NotesSerializer
    permission_classes = [ReadOnly]

    def retrieve(self, request, *args, **kwargs):
        """Returns children of given Note ID"""
        note_id = kwargs.get('pk', None)
        try:
            note = Notes.objects.get(id=note_id)
        except (Notes.MultipleObjectsReturned, Notes.DoesNotExist) as err:
            raise ParseError(err)

        queryset = Notes.get_children(note)
        return Response(NotesSerializer(queryset, many=True).data)


class AttachmentRetrieveView(generics.RetrieveAPIView):
    """Retrieve Attachment View"""
    queryset = Attachment.objects.all()
    serializer_class = AttachmentSerializer
    permission_classes = [ReadOnly]

    def retrieve(self, request, *args, **kwargs):
        attach_id = self.kwargs.get('pk', 0)
        try:
            attachment = Attachment.objects.get(id=attach_id)
        except (Attachment.DoesNotExist, Attachment.MultipleObjectsReturned) as err:
            raise ParseError(err)
        with attachment.file.open() as fh:
            c = magic.from_file(attachment.file.path, mime=True)
            return HttpResponse(fh.read(), content_type=c)
