from django.core.files.storage import FileSystemStorage
from django.utils.deconstruct import deconstructible
from nota.settings.base import DJANGO_DRF_FILEPOND_FILE_STORE_PATH


@deconstructible
class VariableFileSystemStorage(FileSystemStorage):
    """ Custom storage class, otherwise Django assumes all files are uploads headed to `MEDIA_ROOT`.
    Subclassing necessary to avoid messing up with migrations"""
    def __init__(self, **kwargs):
        kwargs.update({
            'location': DJANGO_DRF_FILEPOND_FILE_STORE_PATH,
        })
        super(VariableFileSystemStorage, self).__init__(**kwargs)
