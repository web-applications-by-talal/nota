from django.conf.urls import url
from django.contrib import admin
from django.http import HttpResponse, HttpResponseServerError
from django.urls import path, include
from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema
from drf_yasg.views import get_schema_view
from rest_framework import permissions
from rest_framework.authtoken import views
from django.views.generic import RedirectView

from api.authentication import CustomAuthToken

schema_view = get_schema_view(
    openapi.Info(
        title="Notes REST API",
        default_version='v1',
        description="REST api for the Notes project",
    ),
    public=False,
    permission_classes=(permissions.AllowAny,),)

# Disable authentication for the docs endpoint
schema_view.authentication_classes = []

request_body = openapi.Schema(
    type=openapi.TYPE_OBJECT,
    properties={
        'x': openapi.Schema(type=openapi.TYPE_STRING, description='string'),
        'y': openapi.Schema(type=openapi.TYPE_STRING, description='string'),
    }
)

documented_token_view = \
    swagger_auto_schema(
        method='post',
        request_body=openapi.Schema(
            'auth_body',
            type=openapi.TYPE_OBJECT,
            description='Obtain the token for this user',
            properties={
                'username': openapi.Schema(type=openapi.TYPE_STRING, description='A valid username',
                                           default='admin'),
                'password': openapi.Schema(type=openapi.TYPE_STRING, description='Password for this user',
                                           default='admin'),
            }
        ),
        responses={201: 'Token successfully created',
                   401: 'Unauthorized: The user is not correctly authenticated'}
    )(views.obtain_auth_token)


def liveliness(request):
    return HttpResponse("OK")


def readiness(request):
    try:
        # Connect to db
        return HttpResponse("OK")
    except Exception as e:
        return HttpResponseServerError("db: cannoct connect to database.")


urlpatterns = [
    path('liveness', liveliness, name="liveness"),
    path('readiness', readiness, name="readiness"),
    path(r'', readiness, name="readiness"),
    path('admin/', admin.site.urls),
    path('auth/', include('rest_framework.urls', namespace='rest_framework')),
    path(r'docs/', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
    path(r'api/', include('api.urls')),
    path(r'api/api-token-auth/', CustomAuthToken.as_view()),
    path(r'api', RedirectView.as_view(url='/api/')),
]
