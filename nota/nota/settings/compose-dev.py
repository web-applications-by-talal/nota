from .base import *
import os
# Database
# https://docs.djangoproject.com/en/2.2/ref/settings/#databases

ALLOWED_HOSTS = ['*']

DEBUG = True

# See https://lincolnloop.com/blog/django-logging-right-way/ for more details
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'normal': {
            'format': '%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
        },
    },
    'handlers': {
        'server_log': {
            'class': 'logging.StreamHandler',
            'formatter': 'normal',
        },
    },
    'loggers': {
        # Root logger
        '': {
            'handlers': ['server_log'],
            'level': 'INFO',
        },
        'portal': {
            'handlers': ['server_log'],
            'level': 'INFO',
            'propagate': False,
        },
        'django_auth_ldap': {
            'handlers': ['server_log'],
            'level': 'DEBUG',
            'propagate': False,
        },
    },
}

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': os.getenv('DB_NAME', 'nota'),
        'USER': os.getenv('DB_USER', 'nota'),
        'PASSWORD': os.getenv('DB_PASSWORD', 'nota'),
        'HOST': os.getenv('DB_HOST', 'db'),
        'PORT': os.getenv('DB_PORT', '5432'),
    }
}


# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'DEVSECRETEKEYTOBECHANGE'
