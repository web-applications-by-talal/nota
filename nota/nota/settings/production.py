from .base import *
import os

DEBUG = False

ALLOWED_HOSTS = [os.getenv('APPLICATION_DOMAIN'), os.getenv('POD_IP'), os.getenv('NODE_IP'), os.getenv('EXTERNAL_IP')]

# See https://lincolnloop.com/blog/django-logging-right-way/ for more details
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'normal': {
            'format': '%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
        },
    },
    'handlers': {
        'server_log': {
            'class': 'logging.StreamHandler',
            'formatter': 'normal',
        },
    },
    'loggers': {
        # Root logger
        '': {
            'handlers': ['server_log'],
            'level': 'INFO',
        },
        'portal': {
            'handlers': ['server_log'],
            'level': 'INFO',
            'propagate': False,
        },
        'django_auth_ldap': {
            'handlers': ['server_log'],
            'level': 'DEBUG',
            'propagate': False,
        },
    },
}


DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': os.getenv('DB_NAME'),
        'USER': os.getenv('DB_USER'),
        'PASSWORD': os.getenv('DB_PASSWORD'),
        'HOST': os.getenv('DB_HOST', 'db'),
        'PORT': os.getenv('DB_PORT', '5432'),
    }
}
